namespace Window {
    extern bool Focus;
    extern sf::Window* Window;
    extern int Width, Height;
	void Init();
	void HandleEvents();
	void Clear();
	void Display();
}