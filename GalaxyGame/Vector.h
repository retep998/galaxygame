struct Vector3 {
	double x, y, z;
	Vector3() : x(0), y(0), z(0) {}
	Vector3(double x, double y, double z) : x(x), y(y), z(z) {}
	Vector3(const Vector3& o) : x(o.x), y(o.y), z(o.z) {}
	Vector3 Normalize() {
		double m = sqrt(x*x+y*y+z*z);
		return Vector3(x/m, y/m, z/m);
	}
	Vector3 operator+(Vector3 o) {
		return Vector3(x+o.x, y+o.y, z+o.z);
	}
	Vector3 operator*(double o) {
		return Vector3(x*o, y*o, z*o);
	}
};