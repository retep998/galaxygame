struct Matrix4 {
	double e[16];
	Matrix4() {}
	Matrix4(double e1, double e2, double e3, double e4, double e5, double e6, double e7, double e8, double e9, double e10, double e11, double e12, double e13, double e14, double e15, double e16) {
		e[0] = e1;
		e[1] = e2;
		e[2] = e3;
		e[3] = e4;
		e[4] = e5;
		e[5] = e6;
		e[6] = e7;
		e[7] = e8;
		e[8] = e9;
		e[9] = e10;
		e[10] = e11;
		e[11] = e12;
		e[12] = e13;
		e[13] = e14;
		e[14] = e15;
		e[15] = e16;
	}
	Matrix4(const Matrix4& o) {
		e[0] = o.e[0];
		e[1] = o.e[1];
		e[2] = o.e[2];
		e[3] = o.e[3];
		e[4] = o.e[4];
		e[5] = o.e[5];
		e[6] = o.e[6];
		e[7] = o.e[7];
		e[8] = o.e[8];
		e[9] = o.e[9];
		e[10] =	o.e[10];
		e[11] =	o.e[11];
		e[12] =	o.e[12];
		e[13] =	o.e[13];
		e[14] =	o.e[14];
		e[15] =	o.e[15];
	}
};