#include "Global.h"
GLuint ctex, program, buf;
GLint lcam, lctex;
const GLint num = 0x10000, size = 5;
std::mt19937_64 Engine;
void Sector::Init() {
    //The shader
    program = glCreateProgram();
    glAttachShader(program, glLoadShader("PointStarVertex", GL_VERTEX_SHADER));
    glAttachShader(program, glLoadShader("PointStarFragment", GL_FRAGMENT_SHADER));
    glLinkProgram(program);
    glCheckProgram(program);
	glBindAttribLocation(program, 0, "position");
	glBindAttribLocation(program, 1, "size");
	glBindAttribLocation(program, 2, "temp");
	lcam = glGetUniformLocation(program, "camera");
	lctex = glGetUniformLocation(program, "colortexture");
    //Temperature to color texture
	glGenTextures(1, &ctex);
	glBindTexture(GL_TEXTURE_1D, ctex);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB32F_ARB, 400, 0, GL_RGB, GL_FLOAT, TempColors);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //Vertex buffer
	glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
	GLfloat* data = new GLfloat[size*num];
	std::normal_distribution<GLfloat> dist1(0, Pc);
	std::uniform_real_distribution<GLfloat> dist2(0, 1);
	for (int i = 0; i < size*num; i += size) {
		data[i] = dist1(Engine);
		data[i+1] = dist1(Engine);
		data[i+2] = dist1(Engine);
		GLfloat d = dist2(Engine);
        data[i+3] = 5e9*pow(d, 50);
		data[i+4] = data[i+3]/1.4E5;
	}
	glBufferData(GL_ARRAY_BUFFER, 4*size*num, data, GL_STATIC_DRAW);
	delete data;
}
void Sector::Render() {
    glUseProgram(program);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBindTexture(GL_TEXTURE_1D, ctex);
    glBlendFunc(GL_ONE, GL_ONE);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 3, GL_FLOAT, 0, 4*size, 0);
	glVertexAttribPointer(1, 1, GL_FLOAT, 0, 4*size, (GLvoid*)12);
	glVertexAttribPointer(2, 1, GL_FLOAT, 0, 4*size, (GLvoid*)16);
	glUniform3f(lcam, Camera::p.x, Camera::p.y, Camera::p.z);
	glUniform1i(lctex, 0);
	glDrawArrays(GL_POINTS, 0, num);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}