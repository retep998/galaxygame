#include "Global.h"
namespace Window {
    bool Focus = true;
	sf::Window* Window;
    int Width, Height;
	void Init() {
        sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
		//Window = new sf::Window(sf::VideoMode(desktop.width, desktop.height, desktop.bitsPerPixel), "Galaxy Game", sf::Style::Fullscreen, sf::ContextSettings(32, 0, 0, 3, 3));
        Window = new sf::Window(sf::VideoMode(800, 600, desktop.bitsPerPixel), "Galaxy Game", sf::Style::Titlebar, sf::ContextSettings(32, 0, 0, 3, 3));
        Width  = Window->getSize().x;
        Height = Window->getSize().y;
		Window->setMouseCursorVisible(false);
        Window->setVerticalSyncEnabled(false);
		glewExperimental = true;
		if (glewInit() != GLEW_OK) std::abort();
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
		glClearColor(0, 0, 0, 0);
        glEnable(GL_VERTEX_ARRAY);
        glEnable(GL_BLEND);
        glClampColor(GL_CLAMP_VERTEX_COLOR_ARB,   GL_FALSE);
        glClampColor(GL_CLAMP_FRAGMENT_COLOR_ARB, GL_FALSE);
        glClampColor(GL_CLAMP_READ_COLOR_ARB,     GL_FALSE);
        glClampColor( GL_CLAMP_READ_COLOR,        GL_FALSE);
	}
	double delta;
	void HandleEvents() {
		sf::Event e;
		while (Window->pollEvent(e)) {
			switch (e.type) {
			case sf::Event::Closed:
				std::exit(0);
				break;
			case sf::Event::LostFocus:
				Focus = false;
				break;
			case sf::Event::GainedFocus:
				Focus = true;
				break;
			case sf::Event::KeyPressed:
				switch(e.key.code) {
				case sf::Keyboard::Escape:
					std::exit(0);
					break;
				}
				break;
			}
		}
	}
	void Display() {
		Window->display();
		GLenum e = glGetError();
		switch (e) {
		case GL_NO_ERROR:                                                                       break;
		case GL_INVALID_ENUM:                  std::cout << "Invalid enum"        << std::endl; break;
		case GL_INVALID_VALUE:                 std::cout << "Invalid enum"        << std::endl; break;
		case GL_INVALID_OPERATION:             std::cout << "Invalid operation"   << std::endl; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: std::cout << "Invalid framebuffer" << std::endl; break;
		case GL_OUT_OF_MEMORY:                 std::cout << "Out of memory"       << std::endl; break;
        default:                               std::cout << "Unknown error"       << std::endl; break;
		}
	}
}