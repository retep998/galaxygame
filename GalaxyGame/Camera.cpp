#include "Global.h"

namespace Camera {
	Vector3 p(0, 0, 0);
    Vector3 pv(0, 0, 0);
	Quat r(0, 0, 0, 1);
    Quat rv(0, 0, 0, 1);
	void Update() {
        glLoadIdentity();
        gluPerspective(90, (double)Window::Width/Window::Height, -1, 1);
	    glMultMatrixd(r.Matrix().e);
	    glTranslated(-p.x, -p.y, -p.z);
        if (!Window::Focus) return;
#define KP(x) sf::Keyboard::isKeyPressed(sf::Keyboard::x)
		if (KP(A)) pv = pv+r*Vector3(-1,  0,  0)*Time::Delta*C*20000;
		if (KP(D)) pv = pv+r*Vector3( 1,  0,  0)*Time::Delta*C*20000;
        if (KP(Q)) pv = pv+r*Vector3( 0, -1,  0)*Time::Delta*C*20000;
		if (KP(E)) pv = pv+r*Vector3( 0,  1,  0)*Time::Delta*C*20000;
        if (KP(W)) pv = pv+r*Vector3( 0,  0, -1)*Time::Delta*C*20000;
		if (KP(S)) pv = pv+r*Vector3( 0,  0,  1)*Time::Delta*C*20000;
        if (KP(I)) rv = rv*Quat(r*Vector3(1, 0, 0),  Time::Delta*0.01);
        if (KP(K)) rv = rv*Quat(r*Vector3(1, 0, 0), -Time::Delta*0.01);
        if (KP(J)) rv = rv*Quat(r*Vector3(0, 1, 0),  Time::Delta*0.01);
        if (KP(L)) rv = rv*Quat(r*Vector3(0, 1, 0), -Time::Delta*0.01);
        if (KP(U)) rv = rv*Quat(r*Vector3(0, 0, 1),  Time::Delta*0.01);
        if (KP(O)) rv = rv*Quat(r*Vector3(0, 0, 1), -Time::Delta*0.01);
        if (KP(LShift)) pv = pv*pow(0.1, Time::Delta);
        if (KP(RShift)) rv = rv*pow(0.1, Time::Delta);
        p = p+pv;
        r = rv*r;
	}
}