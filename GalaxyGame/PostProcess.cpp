#include "Global.h"
namespace PostProcess {
    GLuint fbo, tex, program, buf;
    GLint ltex;
    void Init() {
        //The framebuffer
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB, Window::Width, Window::Height, 0, GL_RGBA, GL_HALF_FLOAT_ARB, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glGenFramebuffers(1, &fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) std::abort();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        //The shader
        program = glCreateProgram();
        glAttachShader(program, glLoadShader("PostVertex", GL_VERTEX_SHADER));
        glAttachShader(program, glLoadShader("PostFragment", GL_FRAGMENT_SHADER));
        glLinkProgram(program);
        glCheckProgram(program);
        glBindAttribLocation(program, 0, "vertex");
        ltex = glGetUniformLocation(program, "fbo");
        //The vertex buffer
        glGenBuffers(1, &buf);
        glBindBuffer(GL_ARRAY_BUFFER, buf);
        glBufferData(GL_ARRAY_BUFFER, 64, Rectangle, GL_STATIC_DRAW);
    }
    void Begin() {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glClear(GL_COLOR_BUFFER_BIT);
    }
    void End() {
        glLoadIdentity();
        glOrtho(0, 1, 0, 1, -1, 1);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindTexture(GL_TEXTURE_2D, tex);
        glGenerateMipmap(GL_TEXTURE_2D);
        glUseProgram(program);
        glBindBuffer(GL_ARRAY_BUFFER, buf);
        glBlendFunc(GL_ONE, GL_ZERO);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, 0, 16, 0);
        glUniform1i(ltex, 0);
        glDrawArrays(GL_QUADS, 0, 4);
        glDisableVertexAttribArray(0);
    }
}