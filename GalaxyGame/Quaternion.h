struct Quat {
	double x, y, z, w;
	Quat() : x(0), y(0), z(0), w(0) {}
	Quat(double x, double y, double z, double w) : x(x), y(y), z(z), w(w) {}
	Quat(const Quat& o) : x(o.x), y(o.y), z(o.z), w(o.w) {}
	Quat(Vector3 v, double angle) {
		angle *= 0.5;
		v = v.Normalize();
		double s = sin(angle);
		x = (v.x*s);
		y = (v.y*s);
		z = (v.z*s);
		w = cos(angle);
	}
	Quat Normalize() {
		double m = sqrt(x*x+y*y+z*z+w*w);
		return Quat(x/m, y/m, z/m, w/m);
        
	}
	Quat Conjugate() {
		return Quat(-x, -y, -z, w);
	}
	Quat operator*(Quat o) {
		return Quat(w*o.x+x*o.w+y*o.z-z*o.y, w*o.y+y*o.w+z*o.x-x*o.z, w*o.z+z*o.w+x*o.y-y*o.x, w*o.w-x*o.x-y*o.y-z*o.z);
	}
    Quat operator*(double o) {
        return Quat(x, y, z, w/o).Normalize();
    }
	Vector3 operator*(Vector3 o) {
		o = o.Normalize();
		Quat v(o.x, o.y, o.z, 0);
		v = *this*v*Conjugate();
		return Vector3(v.x, v.y, v.z);
	}
	Matrix4 Matrix() {
		double x2 = x*x;
		double y2 = y*y;
		double z2 = z*z;
		double xy = x*y;
		double xz = x*z;
		double yz = y*z;
		double wx = w*x;
		double wy = w*y;
		double wz = w*z;
		return Matrix4(1-2*(y2+z2), 2*(xy-wz), 2*(xz+wy), 0, 2*(xy+wz), 1-2*(x2+z2), 2*(yz-wx), 0, 2*(xz-wy), 2*(yz+wx), 1-2*(x2+y2), 0, 0, 0, 0, 1);
	}
};