#include "Global.h"
GLuint glLoadShader(std::string name, GLenum type) {
    GLuint s = glCreateShader(type);
    std::ifstream file("Shaders/"+name+".gl");
    std::string text;
    std::getline(file, text, '\0');
    const GLchar* t = text.c_str();
    glShaderSource(s, 1, &t, nullptr);
    glCompileShader(s);
    GLint len;
	glGetShaderiv(s, GL_INFO_LOG_LENGTH , &len);
    GLchar* log = new GLchar[len];
	glGetShaderInfoLog(s, len, 0, log);
	std::cout << log << std::flush;
    delete log;
    GLint compiled;
    glGetShaderiv(s, GL_COMPILE_STATUS, &compiled);
	if (!compiled) std::abort();
    return s;
}
GLuint glCheckProgram(GLuint program) {
    GLint len;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH , &len);
    GLchar* log = new GLchar[len];
	glGetProgramInfoLog(program, len, 0, log);
	std::cout << log << std::flush;
    delete log;
    GLint linked;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked) std::abort();
}