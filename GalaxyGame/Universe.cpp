#include "Global.h"
namespace Universe {
    void Generate() {
        Window::Init();
        PostProcess::Init();
        Sector::Init();
    }
    void Render() {
        Window::HandleEvents();
        Time::Update();
	    Camera::Update();
        PostProcess::Begin();
	    Sector::Render();
        PostProcess::End();
        Window::Display();
    }
}