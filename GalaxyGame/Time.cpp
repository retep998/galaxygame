#include "Global.h"
namespace Time {
    double Delta = 0;
    double FPS = 0;
    void Update() {
        static clock_t last = clock();
	    clock_t now = clock();
	    Delta = static_cast<double>(now-last)/CLOCKS_PER_SEC;
	    last = now;
	    FPS = FPS*(1-Delta) + Delta/std::max(Delta, 0.0001);
        Window::Window->setTitle(std::to_string(FPS));
    }
}